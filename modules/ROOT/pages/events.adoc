include::ROOT:partial$attributes.adoc[]

= Events

The purpose of this page is to build a *Fedora Diversity events calendar* and to identify *regional participation* for each event.
Since the agenda of the Diversity, Equity, & Inclusion team is to assist minorities and promote a more inclusive Fedora community, we are targeting events which are minority-specific.

This page is maintained by the Fedora Diversity, Equity, & Inclusion Team.
In case of any modifications required, please contact the Diversity, Equity, & Inclusion Team or the xref:roles:council-advisor.adoc[DEI Adviser].


[[event-list]]
== Event list

1. **FWD (Fedora Week of Diversity)**
* link:{FWIKI}/Fedora_Women_Day_2016[Fedora Women's Day] was a day to celebrate and thank the women contributors across the Fedora Project.
  It was celebrated every year on (or near) July 15, the anniversary of the Fedora Women project in 2006, until it was expanded to the Fedora Week of Diversity.
* The 2016 event was a success and we would love to host it again.
  We will pre-plan this in advance, create the resources we need to spread it wider to include more people.
  The link for the 2016 page is link:{FWIKI}/Fedora_Women_Day_2016[here].
* Areas to improve on for 2017:
** Communication and coordination with regional Ambassadors
** Reaching out to more local communities ahead of time (including existing teams like Open Labs in Albania and other groups in Greece and India)
*** #idea http://wic.rit.edu/[WiC at RIT]?
** Providing resources / slide decks ahead of time for event organizers (common foundation for local events)
** Time zone planning (WhenIsGood poll could be helpful for planning times with regional organizers for video calls with xref:roles:council-advisor.adoc[DEI Adviser] / other team members
2. **FLISoL**
* https://en.wikipedia.org/wiki/FLISOL[FLISoL], an acronym for Festival Latinoamericano de Instalación de Software Libre (Latin American free software install fest), is the biggest event for spreading Software Libre in Latin America since 2005.
  It is performed simultaneously in different countries of Latin America.
  In 2012, more than 290 cities from
  20 countries participated on a FLISoL.
* Our Fedora xref:roles:council-advisor.adoc[DEI Adviser], link:{FWIKI}/User:Jonatoni[Jonatoni], will make some efforts to put a Fedora Diversity talk in as many locations as she can.
  This will help target the "Latam-Hispanic" group.
  We can align our regional / local events to support this.
3. **LGBT community**
* To raise awareness, we will also hold some activities for the LGBT community.
  However, since the worldwide celebration of International Day Against Homophobia, Transphobia, and Biphobia would be on Wednesday, May 17, 2017, we could do it on *Saturday, May 20, 2017*.
  A panel like we did for FWD might be fantastic.
4. **Disabilities**
* We will include a day to celebrate those who, despite the adversity, have conquered their disabilities to work with us at Fedora.
  The International Day of Persons with Disabilities will be next on *Saturday, December 3, 2017*.
  We could host a panel where they show us how they have "conquered the IT world".
5. **Fedora Diversity Day / FAD**
* We would like to initiate recognizing a Fedora Diversity Day.
  We would love this to be a yearly FAD or something that we can consider a Diversity, Equity, & Inclusion team's own event to review numbers, strategies, and so on.
  We are sure that we will work on having a FAD to match our goals and we will be able to have a proper diversity (general) event.
  Link for this is link:{FWIKI}/FAD_Diversity_2017[here].
6. **Diversity Panel**
* The next Diversity Panel planning in process.
  More on this is https://pagure.io/fedora-diversity/issue/10[here] in the ticket.
7. **Diversity, Equity, & Inclusion Team presence at Flock 2017**
* Fedora Diversity Panel, and…
* Women in Open Source
* More information on Flock 2016 panels and talks can be found on these blog posts:
** https://whatamithinks.wordpress.com/2016/08/15/480/
** https://blog.justinwflory.com/2016/08/fedora-flock-2016/


[[links]]
== Links

1.  https://en.wikipedia.org/wiki/International_Day_Against_Homophobia,_Transphobia_and_Biphobia
2.  https://en.wikipedia.org/wiki/United_Nations%27_International_Day_of_Persons_with_Disabilities


[[additional-information]]
== Additional information

* link:{FWIKI}/How_to_organize_a_Fedora_event[Organization] -- event organization, budget information, and regional responsibility.
* link:Event_reports[Event reports] -- guidelines and suggestions.
* link:Event_Wiki_Template[Event Wiki Template] -- template for Event pages
