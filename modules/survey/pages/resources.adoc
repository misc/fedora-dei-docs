include::ROOT:partial$attributes.adoc[]

= Resources & references

Several other existing surveys, Open Source research studies, and other writing influenced the creation, design, and execution of this survey:

[NOTE]
====
FLOSS Survey 2013 focuses on a comparative analysis of different groups of respondents instead of asserting authoritative statistics abour diversity in the Open Source community.
====

FLOSS Survey 2013::
* https://floss2013.larjona.net/[Questions and results]
* http://flosshub.org/content/women-freelibreopen-source-software-situation-2010s[Women in Free/Libre/Open Source Software: The Situation in the 2010s] paper with results analysis

Apache Software Foundation Committer Diversity Survey::
* http://mail-archives.apache.org/mod_mbox/www-announce/201611.mbox/%3Cpony-31e1cbf2b23a01ea035ee3323fe2ab95950c8284-dc1f345f30800e5cbef086a8801c55be77bc49c3%40announce.apache.org%3E[Announcement e-mail]
* https://cwiki.apache.org/confluence/display/COMDEV/ASF+Committer+Diversity+Survey+-+2016[Results]
