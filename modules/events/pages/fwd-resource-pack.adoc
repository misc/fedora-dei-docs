include::ROOT:partial$attributes.adoc[]

= FWD organizer resource pack

In this page you can find a resource pack that helps in organizing a successful xref:events:about-fwd.adoc[Fedora Week of Diversity] event. The points below can be used in the event organizer's agenda:

    * How to welcome contributors to Fedora
    * A summary about Fedora D.E.I. team
    * Overview of different Fedora editions
    * Fedora and Python

== How to welcome new contributors to Fedora?

Did you come across folks who are interested in contributing to the Fedora project? Then please create link:https://pagure.io/fedora-join/Welcome-to-Fedora/issues[Welcome to Fedora ticket] for the newcomers with Fedora Join SIG.  This ticket helps them to easily track their progress in the journey to becoming Fedora contributors.
Find more information about the welcome ticket process link:https://docs.fedoraproject.org/en-US/fedora-join/welcome/welcome/[here].

Need help with creating the welcome ticket? Reach out to Fedora Join SIG at any of their https://docs.fedoraproject.org/en-US/fedora-join/#communication[Communication channels].

== About Fedora Diversity, Equity, & Inclusion Team

At your event, start with an introduction to the Fedora Diversity, Equity, & Inclusion Team.
Explain the role of D.E.I. team in the Fedora community and its upcoming goals.
Share our xref:ROOT:index.adoc[team's introduction page] with anyone who is interested in learing more.

What is the role of Fedora Diversity, Equity, & Inclusion Team?::
As the name indicates, the main goal of the D.E.I. team is to foster diversity, equity, and inclusion in Fedora community.
Below are some of the many efforts focused by D.E.I. team to achieve its goal:
* Creating content and organizing events to spread awareness about diversity within and outside Fedora community (like Fedora Week of Diversity!)
* Supporting community building and engagement activities to promote inclusion
* Coordinate with Fedora sub-projects to enhance diversity, equity, and inclusion in the community
* Supporting programs committed to building diversity, equity, and inclusion in Free and Open Source Software communities

What the D.E.I. team works on?::
The D.E.I. team works on various tasks to support diversity, equity, and inclusion in the Fedora Project.
For example, new additions were made to the 2018 https://flocktofedora.org/[Fedora Flock conference] such as including gender-neutral bathrooms, a quiet room, and communication preference stickers.
These additions in turn supported our existing effort to draft https://pagure.io/fedora-diversity/issue/54[event inclusion guidelines] for Fedora event organizers.
These guidelines when shared within and outside Fedora community provide best practices to organizers holding an event.
In the past, we also worked on a xref:survey:index.adoc[Fedora Project Demographic Survey] to understand who is in the Fedora Community.

Community Script::
Hi, my name is \_____, I am from \_____, I am \_____, and I speak \_____.
We are from different countries, We speak different languages, We are of different cultures, But Fedora unites us with Open Source. WE ARE FEDORA!

== Fedora Editions

{FWIKI}/Editions[Fedora Editions] are the flagship configurations of Fedora offered by the Fedora Project.
They are promoted on the https://getfedora.org/[getfedora.org] website.
Each Edition has a different use case and audience as explained below.

=== Fedora Workstation

https://getfedora.org/workstation/[Fedora Workstation] is a primary desktop configuration offered by Fedora.
Fedora workstation is a great choice for someone looking to use a Linux on their desktop or learn to develop in a Linux environment.
It ships the https://www.gnome.org/[GNOME Desktop Environment], includes latest versions of programming languages and libraries, and is easy to make a https://fedoramagazine.org/make-fedora-usb-stick/[bootable USB stick].

* Check out this overview of notable features in Fedora's recent release link:https://fedoramagazine.org/announcing-fedora-{MAJOROSVER}/[here].

* For a comprehensive description of updates and new features, see the official release notes link:https://docs.fedoraproject.org/en-US/fedora/f{MAJOROSVER}/release-notes/[here].

=== Fedora Server

https://getfedora.org/server/[Fedora Server] is a primary server configuration offered by Fedora.
This makes a good choice for seasoned system administrators, allowing them to work with latest technologies from the open source community.
This is a short-lifecycled, community-supported server environment.
Fedora Server features https://cockpit-project.org/[Cockpit], https://www.freeipa.org/page/About[FreeIPA identity management], and the brand-new https://docs.pagure.org/modularity/[Fedora Server Modular Edition].

See recent features to Fedora Server {FWIKI}/Fedora_{MAJOROSVER}_talking_points#Fedora_Server[here].

=== Fedora Silverblue

https://silverblue.fedoraproject.org/[Fedora Silverblue] is a variant of Fedora Workstation.
It looks, feels and behaves like a regular desktop operating system giving an experience is similar to a standard Fedora Workstation.

However, unlike other operating systems, Silverblue is immutable.
This means that every installation is identical to every other installation of the same version.
The operating system that is on disk is exactly the same from one machine to the next, and it never changes as it is used.

=== Fedora CoreOS

https://getfedora.org/en/coreos/[Fedora CoreOS] is an automatically updating, minimal, monolithic, container-focused operating system designed for clusters but also operable standalone. This is optimized for Kubernetes but also works great without it.
It aims to combine the best of both CoreOS Container Linux and Fedora Atomic Host, by integrating Ignition technology from Container Linux with rpm-ostree and SELinux hardening from Project Atomic.
The goal of this edition is to provide the best container host to run containerized workloads securely and at scale.

=== Fedora IoT

xref:iot:ROOT:index.adoc[Fedora IoT] or Fedora Internet of Things provides a strong foundation for IoT ecosystems.
Whether you’re working on a project at home, industrial gateways, smart cities or analytics with AI/ML, Fedora IoT can provide a trusted open source platform to build on.
Fedora IoT rolling releases help you keep your ecosystem up-to-date.

== Fedora Objectives

[quote, 'From https://docs.fedoraproject.org/en-US/project/objectives/[Fedora Objectives]']
____
The primary role of the Fedora Council is to identify the short, medium, and long term goals of the Fedora community and to organize and enable the project to best achieve them.
Each objective has a designated Objective Lead who is responsible for coordinating efforts to reach the Objective’s goals, for evaluating and reporting on progress, and for working regularly with all relevant groups in Fedora to ensure that progress is made.
____

These Objectives are listed as follows:

* xref:minimization:index.adoc[Fedora Minimization]
* {FWIKI}/Objectives/Fedora_IoT[Internet of Things (IoT)]

== Fedora <3 Python

https://fedoralovespython.org/[*fedoralovespython.org*]::
Collection of resources on using Python on Fedora.
https://developer.fedoraproject.org/tech/languages/python/python-installation.html[*Python - Fedora Developer Portal*]::
Steps and guides on how to install, use, and develop with Python on Fedora (including Django, Flask, https://developer.fedoraproject.org/tech/languages/python/scipy.html[SciPy] and more).
http://fedora.portingdb.xyz/[*Python 3 porting database*]::
Fedora's Python 3 porting database.
Shows progress towards fully migrating Fedora Python packages to Python 3.
https://labs.fedoraproject.org/python-classroom/[*Python Classroom Lab*]::
Fedora Lab intended for teachers and instructors teaching students Python.
It includes several Python development tools and libraries not normally installed by default (also is available as GNOME desktop, a Vagrant VM, or a Docker container).
https://whatcanidoforfedora.org/en/#coding#python[*Contributing to Python in Fedora*]::
Helpful resources to find ways to contribute to Python efforts in Fedora.
https://fedoramagazine.org/tag/python/[*Python articles on Fedora Magazine*]::
All articles on the Fedora Magazine related to Python in Fedora.
Two great examples are https://fedoramagazine.org/python-3-7-now-available-in-fedora/[_Python 3.7 now available in Fedora_] and https://fedoramagazine.org/install-pipenv-fedora/[_How to install Pipenv on Fedora_].

