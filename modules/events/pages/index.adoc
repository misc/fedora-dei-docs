include::ROOT:partial$attributes.adoc[]

= Events

The Events section of the {team_name} Docs aims to build a *Fedora Diversity events calendar* and identify *regional events for participation*.
As the DEI team aims to assist minorities and promote a more inclusive environment in the Fedora Community, we target minority-specific events.

This page is maintained by the {team_name}.
For suggesting any modifications, xref:ROOT:get-involved.adoc[contact the team] or the xref:roles:council-advisor.adoc[DEI Adviser].
